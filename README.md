### Hosted on

[sf-movies](https://sf-movies.netlify.com/)

### Description of the problem and solution.

This single page application provides a service that shows on a map where movies have been filmed in San Francisco.
There are two main layout parts - component with input and output data and Google map. Tere are autocomplition for input field and navigation by keyboard.

### Architecture solution

This application was written in ES6, I use Fountain js yeoman generator to generate the basic skeleton. Current stack is - ES6, Babel, React, Redux, webpack.

I was focused on the front-end track solution with caching optimizations from external REST APIs such as main data with all movies, data from previously searched movies, such as poster information,
and geocoded markers values.

I used the React & Redux as the main toolset in my application. This allows me to separate application for small pieces, keep code clarity and allows to quickly expand functionality.

For the visual perspective, I choose the material light framework, as it provides a nice visual experience.

The layout was done by using flex containers.

I reuse the existing react autocomplete widget with movies data.

Note: I chose the Front-end track I used the Google geocoder API which is a bit approximate.


### Trade-offs and TODOs:

I've added a feature toggle for unfinished implementation of the switching being the type of sortings, by year, by name of director, etc.

Currently, supported sortings are by movie name.

Add localStore revalidation. Currently, I rely on that fact that geocoded data and poster data will not change over time,
but it's a good practice to have some invalidation mechanism, for instance by timestamp.

Add some e2e test for the main user flows to be able to make improvements and refactoring more safety.

Add more unit tests and mock modules to cover main functionality.

Adjust overall styling add fitting all possible text, add ellipsis etc. Add links to the IMDB etc.

Improve media queries and add full functional support for the mobile devices.

Improve visual and functional fallbacks for the legacy browsers.

Move to jest test infrastructure from karma completely


### Installation and serve from local:

``` npm i ```

to serve application from localhost:

``` npm run serve ```

to run test and get the coverage:

``` npm run jest ```
and open the [link](http://localhost:3000)

### My profile

[LinkedIn](https://www.linkedin.com/in/ihorpavlenko)
